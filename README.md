# Description
This is a javascript 🟨 project kata 🥋 to learn, practice asynchronism in javascript

![Github](https://github.com/zearkiatos/javascript-asynchronism--kata/actions/workflows/action.yml/badge.svg)

# Made with
[![JavaScript](https://img.shields.io/badge/javascript-ead547?style=for-the-badge&logo=javascript&logoColor=white&labelColor=000000)]()